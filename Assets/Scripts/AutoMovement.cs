﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoMovement : MonoBehaviour {

    public Vector3 speed = Vector3.right;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (GameManager.Instance.gameIsPlaying == false)
        {
            return;
        } 

        transform.position += speed * Time.deltaTime;
	}
}
