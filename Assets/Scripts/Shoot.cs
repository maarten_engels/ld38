﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot : MonoBehaviour {

    public float velocity = 8;
    public Rigidbody projectilePrefab;

    public float delay = 0.1f;
    float delayTimer = 0;

    // Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (GameManager.Instance.gameIsPlaying == false)
        {
            return;
        }

        delayTimer -= Time.deltaTime;

        if (Input.GetButtonDown("Fire1") && delayTimer <= 0)
        {
            Rigidbody rb = Instantiate(projectilePrefab) as Rigidbody;
            rb.transform.position = transform.position;
            rb.transform.rotation = transform.rotation;
            rb.AddForce(transform.forward * velocity, ForceMode.Impulse);
            delayTimer = delay;
        }

    }
}
