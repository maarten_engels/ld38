﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoShoot : MonoBehaviour {

    public float delay = 0.2f;
    public float initialDelay = 1f;

    public float velocity = 8f;

    public Rigidbody projectilePrefab;

    float delayTimer = 0;

	// Use this for initialization
	void Start () {
        delay = initialDelay;
	}
	
	// Update is called once per frame
	void Update () {
        if (GameManager.Instance.gameIsPlaying == false)
        {
            return;
        }

        delayTimer -= Time.deltaTime;
		if (delayTimer <= 0)
        {
            Rigidbody rb = Instantiate(projectilePrefab) as Rigidbody;
            rb.transform.position = transform.position;
            rb.transform.rotation = transform.rotation;

            rb.AddForce(transform.forward * velocity, ForceMode.VelocityChange);
            delayTimer = delay;
        }
	}


}
