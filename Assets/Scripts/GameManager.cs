﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    public static GameManager Instance;
    public static bool autoStart = false;

    public int score = 0;
    public int highScore = 0;

    public bool gameIsPlaying = false;
    public GameObject player;

    public System.Action OnGameStart;
    public System.Action OnGameWon;
    public System.Action OnGameLost;

    private void Awake()
    {
        Instance = this;
    }

    // Use this for initialization
    void Start () {
        if (PlayerPrefs.HasKey("HighScore"))
        {
            highScore = PlayerPrefs.GetInt("HighScore");
        }

        if (autoStart)
        {
            StartGame();
        }
	}

    public void StartGame() {
        GetComponent<AudioSource>().Play();

        gameIsPlaying = true;

        if (OnGameStart != null)
        {
            OnGameStart();
        }

        
        autoStart = false;
    }
	
    public void Win()
    {
        if (gameIsPlaying)
        {
            Debug.Log("You won.");
            gameIsPlaying = false;

            PlayerPrefs.SetInt("HighScore", highScore);

            if (OnGameWon != null)
            {
                OnGameWon();
            }
        }
    }

	// Update is called once per frame
	void Update () {
        if (player == null && gameIsPlaying)
        {
            gameIsPlaying = false;
            PlayerPrefs.SetInt("HighScore", highScore);
            if (OnGameLost != null)
            {
                OnGameLost();
            }
        }
	}

    

    public void AddScore(int scoreToAdd)
    {
        if (gameIsPlaying == false)
        {
            return;
        } 

        score += scoreToAdd;
        if (score > highScore)
        {
                
                highScore = score;
         
        }
    }

    public void RestartGame() 
    {
        autoStart = true;
        SceneManager.LoadScene(0);
    }
}
