﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOnCollision : MonoBehaviour {

    public GameObject gib;
    public string ignoreTag = "DontDestroy";
    public int scoreValue = 0;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnCollisionEnter(Collision collision)
    {
        //Debug.Log(gameObject.name + " collided with: " + collision.gameObject.name);

        if (collision.collider.tag == ignoreTag)
        {
            return;
        }

        if (gib)
        {
            GameObject go = Instantiate(gib) as GameObject;
            go.transform.position = transform.position;
            go.transform.rotation = transform.rotation;
        }

        GameManager.Instance.AddScore(scoreValue);
        Destroy(gameObject);
    }
}
