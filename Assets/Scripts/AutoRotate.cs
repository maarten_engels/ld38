﻿/// <summary>
/// AutoRotate.cs
/// (c) Maarten Engels
/// Created for Ludum Dare 38
/// License: CC BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/)
/// </summary>

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoRotate : MonoBehaviour {

    public Vector3 speed;
    public Space space = Space.World;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        transform.Rotate(speed * Time.deltaTime, space);
	}
}
