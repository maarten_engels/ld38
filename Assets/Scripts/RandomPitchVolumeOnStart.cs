﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomPitchVolumeOnStart : MonoBehaviour {

	// Use this for initialization
	void Start () {
        AudioSource audioSource = GetComponent<AudioSource>();
        if (audioSource != null)
        {
            audioSource.pitch = Random.Range(0.75f, 1.25f);
            audioSource.volume = Random.Range(0.2f, 0.5f);
        }
	}
	
	
}
