﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateObjectOnWin : MonoBehaviour {

    public GameObject[] objectsToActivate;

	// Use this for initialization
	void Start () {
        GameManager.Instance.OnGameWon += ActivateObjects;
	}
	
	void ActivateObjects()
    {
        foreach (GameObject go in objectsToActivate)
        {
            go.SetActive(true);
        }
    }
}
