﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunControl : MonoBehaviour {

    public GameObject[] guns;
    public int currentGun = 0;

    public System.Action OnChangeGun;

	// Use this for initialization
	void Start () {
        ActivateGun(currentGun);
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetButtonDown("Fire2"))
        {
            currentGun += 1;
            currentGun = currentGun % guns.Length;
            ActivateGun(currentGun);
        }
	}

    void ActivateGun(int gun)
    {
        for (int i=0; i < guns.Length; i++)
        {
            if (gun == i)
            {
                guns[i].SetActive(true);
            } else
            {
                guns[i].SetActive(false);
            }
        }
        if (OnChangeGun != null)
        {
            OnChangeGun();
        }
    }
}
