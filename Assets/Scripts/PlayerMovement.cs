﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

    public float speed = 1.0f;
    public Vector2 upperRightBound;
    public Vector2 lowerLeftBound;

    // Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {

        if (GameManager.Instance.gameIsPlaying == false)
        {
            return;
        }

        Vector3 localDisplacement = new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"), 0) * speed * Time.deltaTime;

        Vector3 newLocalPosition = transform.localPosition + localDisplacement;

        if (newLocalPosition.x < lowerLeftBound.x)
        {
            localDisplacement.x = 0;
        }

        if (newLocalPosition.x > upperRightBound.x)
        {
            localDisplacement.x = 0;
        }

        if (newLocalPosition.y < lowerLeftBound.y)
        {
            localDisplacement.y = 0;
        }

        if (newLocalPosition.y > upperRightBound.y)
        {
            localDisplacement.y = 0;
        }


        transform.position += localDisplacement;
	}
}
