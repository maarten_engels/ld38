﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Construct : MonoBehaviour {

    public float constructTime = 1f;

    List<Rigidbody> blocks;
    Dictionary<Rigidbody, Vector3> blocksOriginalPosition;
    Dictionary<Rigidbody, Vector3> blocksDeconstructPosition;

    bool isConstructing = false;

    public GameObject[] objectsToActiveAfterConstruction;

    // Use this for initialization
    void Start () {
        blocksOriginalPosition = new Dictionary<Rigidbody, Vector3>();
        blocksDeconstructPosition = new Dictionary<Rigidbody, Vector3>();
        for (int i=0; i < transform.childCount; i++)
        {
            Rigidbody rb = transform.GetChild(i).GetComponent<Rigidbody>();
            blocksOriginalPosition.Add(rb, transform.GetChild(i).position);
            blocksDeconstructPosition.Add(rb, new Vector3(rb.transform.position.x, 20, rb.transform.position.z));
        }

        Deconstruct();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void Deconstruct()
    {
        foreach (Rigidbody rb in blocksOriginalPosition.Keys)
        {
            rb.isKinematic = true;
            rb.transform.position = blocksDeconstructPosition[rb];
        }
    }

    IEnumerator ConstructBlocks()
    {
        if (isConstructing == false)
        {



            isConstructing = true;
            float t = 0;
            while (t < constructTime)
            {
                foreach (Rigidbody rb in blocksOriginalPosition.Keys)
                {
                    if (rb != null)
                    {
                        rb.transform.position = (Vector3.Lerp(blocksDeconstructPosition[rb], blocksOriginalPosition[rb], t / constructTime));
                    }

                }
                t += Time.deltaTime;
                yield return null;
            }

            foreach (Rigidbody rb in blocksOriginalPosition.Keys)
            {
                if (rb != null)
                {
                    rb.transform.position = (blocksOriginalPosition[rb]);
                }
            }

            yield return null;

            foreach (Rigidbody rb in blocksOriginalPosition.Keys)
            {
                if (rb != null)
                {
                    rb.velocity = Vector3.zero;
                }
            }

            yield return null;

            foreach (Rigidbody rb in blocksOriginalPosition.Keys)
            {
                if (rb != null)
                {
                    rb.isKinematic = false;
                }
            }

            foreach (GameObject go in objectsToActiveAfterConstruction)
            {
                if (go != null)
                {
                    go.SetActive(true);
                }
            } 
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log("Collided with: " + other.name);
        if (other.tag == "TriggerConstruction")
        {
            StartCoroutine("ConstructBlocks");
        }
    }
}
