﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreOnCollision : MonoBehaviour {

    public int pointsPerCollision = 7;

    AudioSource audioSource;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (GameManager.Instance.gameIsPlaying == false)
        {
            return;
        }

        GameManager.Instance.AddScore(pointsPerCollision);
        audioSource.pitch = Random.Range(0.75f, 1.50f);
        audioSource.volume = Random.Range(0.2f, 0.5f);
        audioSource.Play();
    }

    // Update is called once per frame
    void Update () {
		
	}
}
