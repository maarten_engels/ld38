﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MenuManager : MonoBehaviour {

    public GameObject inGameMenu;

    public Button startGameButton;
    public Button resumeGameButton;
    public Button restartGameButton;

    public Text winText;

    bool isPaused = false;

    // Use this for initialization
    void Start () {
        GameManager.Instance.OnGameStart += HideMenu;
        GameManager.Instance.OnGameLost += ShowLoseMenu;
        GameManager.Instance.OnGameWon += ShowWinMenu;
        ShowDefaultMenu();
	}

    private void OnDisable()
    {
        Time.timeScale = 1.0f;
    }

    // Update is called once per frame
    void Update () {
		if (Input.GetButtonDown("Pause") && GameManager.Instance.gameIsPlaying)
        {
            if (isPaused)
            {
                Unpause();
            } else
            {
                Time.timeScale = float.Epsilon;
                ShowPauseMenu();
                isPaused = true;
            }
        }
	}

    public void Unpause()
    {
        Time.timeScale = 1.0f;
        HideMenu();
        isPaused = false;
    }

    void ShowDefaultMenu()
    {
        inGameMenu.SetActive(true);
        winText.gameObject.SetActive(false);
        startGameButton.gameObject.SetActive(true);
        resumeGameButton.gameObject.SetActive(false);
        restartGameButton.gameObject.SetActive(false);
        StartCoroutine(ChooseDefaultButton(startGameButton));
    }

    void HideMenu() 
    {
        inGameMenu.SetActive(false);
        winText.gameObject.SetActive(false);
        startGameButton.gameObject.SetActive(false);
        resumeGameButton.gameObject.SetActive(false);
        restartGameButton.gameObject.SetActive(true);
    }

    void ShowLoseMenu()
    {
        inGameMenu.SetActive(true);        
        winText.gameObject.SetActive(true);
        winText.text = "Game Over";
        startGameButton.gameObject.SetActive(false);
        resumeGameButton.gameObject.SetActive(false);
        restartGameButton.gameObject.SetActive(true);
        StartCoroutine(ChooseDefaultButton(restartGameButton));
    }

    void ShowPauseMenu()
    {
        inGameMenu.SetActive(true);
        winText.gameObject.SetActive(true);
        winText.text = "Paused";
        startGameButton.gameObject.SetActive(false);
        resumeGameButton.gameObject.SetActive(true);
        restartGameButton.gameObject.SetActive(false);
        StartCoroutine(ChooseDefaultButton(resumeGameButton));
    }

    void ShowWinMenu()
    {
        inGameMenu.SetActive(true);
        winText.gameObject.SetActive(true);
        winText.text = "FINISH";
        startGameButton.gameObject.SetActive(false);
        resumeGameButton.gameObject.SetActive(false);
        restartGameButton.gameObject.SetActive(true);
        StartCoroutine(ChooseDefaultButton(restartGameButton));
    }

    IEnumerator ChooseDefaultButton(Button defaultButton)
    {
        EventSystem.current.SetSelectedGameObject(null);
        yield return null;
        EventSystem.current.SetSelectedGameObject(defaultButton.gameObject);
    }
}
