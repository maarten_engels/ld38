﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class UI_ShowCurrentProjectileType : MonoBehaviour {

    public GunControl player;

    public Sprite[] projectileSprites;

    public float autoHideDelay = 2f;

    public Image projectileImage;

	// Use this for initialization
	void Start () {
        GameManager.Instance.OnGameStart += ShowCurrentProjectile;
        GameManager.Instance.OnGameLost += HideProjectileGraphics;
        GameManager.Instance.OnGameWon += HideProjectileGraphics;

        player.OnChangeGun += ShowCurrentProjectile;

        HideProjectileGraphics();
	}
	
    void ShowCurrentProjectile()
    {
        if (GameManager.Instance.gameIsPlaying == false)
        {
            return;
        }

        StopAllCoroutines();

        for (int i=0; i < projectileSprites.Length; i++)
        {
            if (i == player.currentGun)
            {
                projectileImage.sprite = projectileSprites[i];
            } 
        }

        GetComponent<CanvasGroup>().alpha = 1;

        StartCoroutine(FadeOut());
    } 

    void HideProjectileGraphics()
    {
        GetComponent<CanvasGroup>().alpha = 0;
    }

    IEnumerator FadeOut()
    {
        yield return new WaitForSeconds(1f);
        float t = 0;
        while (t < 1)
        {
            GetComponent<CanvasGroup>().alpha = Mathf.Lerp(1, 0, t);
            t += Time.deltaTime;
            yield return null;
        }
        GetComponent<CanvasGroup>().alpha = 0;
    }
}
