# README #

## UFO in a box of bricks ##

* Theme: A Small World ;)
* Game created from scratch in 24 hours by Maarten Engels (maarten@thedreamweb.eu) for Ludum Dare 38.
* Licensed Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0) (https://creativecommons.org/licenses/by-sa/4.0/)

### About the game ###
You are an all powerful alien. You shoot projectiles all around. You obliterate whichever is obstructing your progress.
You are also only 2cm tall, so even this box with toy blocks feels like a vast world to you. 
Can you survive it to the end?

* Use WASD / Arrow keys / Left analog stick to move your ship
* Use SPACE / Button A to shoot
* Use Left-ALT / Button B to switch projectile size
* XBOX 360 controller supported on Windows

### About the entry ###
This game was created using the following tooling: 

* Unity3D /w ProBuilder
* 3D models: Blender
* 2D images: Adobe Photoshop
* Sound effects: ChipTone (http://sfbgames.com/chiptone/)
* Music: GarageBand
* BPDots font: Creative Commons Attribution-No Derivative Works 3.0 Unported (http://creativecommons.org/licenses/by-nd/3.0/) http://www.fontsquirrel.com/license/BPdots

### Known Issues ###
* The left-alt key can show a context sensitive window in your browser. :(

### This repository ###
* This repository requires git lfs (large file support)!
* Contains work in progress version so no guarantees it works;
* You can use the assets (scripts, "art", music / noise, ...) as you like within the limits of the License;
* A WebGL build can be found in the "Stable Builds" folder.